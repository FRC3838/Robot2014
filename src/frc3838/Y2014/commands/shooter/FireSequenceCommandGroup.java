package frc3838.Y2014.commands.shooter;


import edu.wpi.first.wpilibj.command.CommandGroup;
import frc3838.Y2014.commands.time.SleepSecondsCommand;



public class FireSequenceCommandGroup extends CommandGroup
{
    public FireSequenceCommandGroup()
    {
        super();
        addCommands();
    }


    public FireSequenceCommandGroup(String name)
    {
        super(name);
        addCommands();
    }


    protected void addCommands()
    {
        addSequential(new FireCommand());
        addSequential(new SleepSecondsCommand(1));
        addSequential(new RetractCommand());
    }
}