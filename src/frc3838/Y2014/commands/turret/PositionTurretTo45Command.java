package frc3838.Y2014.commands.turret;


public class PositionTurretTo45Command extends AbstractPositionTurretCommand
{
    
    
    protected double getTargetPosition()
    {
        return 45.0;
    }
}
