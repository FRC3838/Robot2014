package frc3838.Y2014.subsystems;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.commands.drive.DriveCommand;
import frc3838.Y2014.controls.DIO;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.PWMChannels;
import frc3838.Y2014.controls.SDKeys;
import frc3838.Y2014.utils.LOG;



public class DriveTrainSubsystem extends Subsystem
{

    private static final DriveTrainSubsystem singleton = new DriveTrainSubsystem();


    private static final double deadZoneThreshold = 0.05;

    private static final boolean invertLeftSideA = true;
    private static final boolean invertLeftSideB = true;
    private static final boolean invertRightSideA = false;
    private static final boolean invertRightSideB = true;

    private static final boolean reverseLeftEncoderDirection = false;
    private static final boolean reverseRightEncoderDirection = true;

    private static ControlMode controlMode = ControlMode.ARCADE;

    private static boolean decreaseSensitivityAtLowerSpeeds = false;

    private static RobotDrive robotDrive;

    private DigitalInput leftEncDigitalInputA;
    private DigitalInput leftEcnDigitalInputB;
    private DigitalInput rightEcnDigitalInputA;
    private DigitalInput rightEcnDigitalInputB;
    private static Encoder leftEncoder;
    private static Encoder rightEncoder;

    /*
        See page 7 of http://files.andymark.com/E4P_datasheet.pdf
        Encoder Model #
        E4P-250-250-N-S-D-D-B
            CPR =       250 cycles per revolution
            Bore =      250 == 1/4"
            Index =     N == No Index
            Output =    S == Single Ended Output
            Cover =     D == Default Cover (as apposed to "Hole in cover")
            Base =      D == Default
            Packaging = B == Bulk packaged

        We have 4inch wheels
     */

    private static final double DISTANCE_PER_PULSE = (4 * Math.PI) / 250;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new DriveTrainSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static DriveTrainSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     */
    private DriveTrainSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            LOG.debug("DriveTrainSubsystem constructor called");
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in DriveTrainSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (EM.isDriveTrainSubsystemEnabled)
        {
            try
            {
                LOG.debug("Initializing DriveTrainSubsystem");

                initRobotDrive();
                initEncoder();

                updateSensitivityModeDisplay();

                LOG.debug("DriveTrainSubsystem initialization completed successfully");
            }
            catch (Exception e)
            {
                EM.isDriveTrainSubsystemEnabled = false;
                LOG.error("An exception occurred in DriveTrainSubsystem.init()", e);
            }

        }
        else
        {
            LOG.info("DriveTrainSubsystem is disabled and will not be initialized");
        }
    }


    private void initEncoder()
    {
        try
        {
            if (EM.isDriveTrainSubsystemEncodersEnabled)
            {
                LOG.info("Initializing drive train Encoders");


                leftEncDigitalInputA = new DigitalInput(DIO.LEFT_ENCODER_CH_A);
                leftEcnDigitalInputB = new DigitalInput(DIO.LEFT_ENCODER_CH_B);

                rightEcnDigitalInputA = new DigitalInput(DIO.RIGHT_ENCODER_CH_A);
                rightEcnDigitalInputB = new DigitalInput(DIO.RIGHT_ENCODER_CH_B);

                leftEncoder = createAndInitEncoder(leftEncDigitalInputA, leftEcnDigitalInputB, reverseLeftEncoderDirection);
                rightEncoder = createAndInitEncoder(rightEcnDigitalInputA, rightEcnDigitalInputB, reverseRightEncoderDirection);
            }
            else
            {
                LOG.info("Drive train Encoders are not enabled and will not be initialized");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.initEncoder()", e);
        }
    }

    private Encoder createAndInitEncoder(DigitalInput digitalInputA, DigitalInput digitalInputB, boolean reverse)
    {
        Encoder encoder = new Encoder(digitalInputA, digitalInputB, reverse);
        encoder.setDistancePerPulse(DISTANCE_PER_PULSE);
        encoder.reset();
        encoder.start();

        return encoder;
    }
    /**
     * Gets the leftEncoder but <strong>MAY RETURN NULL</strong>. Callers should wrap use of this method in
     * <pre>
     *     if (EM.isDriveTrainSubsystemEncodersEnabled)
     *     {
     *         Encoder driveEncoder = driveTrainSubsystem.getEncoder();
     *         driveEncoder.someMethod();
     *     }
     * </pre>
     *
     * @return the drive train leftEncoder <strong>or null</strong> if EM.isDriveTrainSubsystemEncodersEnabled is false
     */
    public Encoder getEncoder()
    {
        return leftEncoder;
    }


    private void initRobotDrive()
    {
        try
        {
            if (isEnabled())
            {
                LOG.trace("initializing robotDrive");
                if (EM.isTestBot)
                {
                    robotDrive = new RobotDrive(PWMChannels.LEFT_DRIVE_MOTOR_A, PWMChannels.RIGHT_DRIVE_MOTOR_A);
                    robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, false);
                    robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
                }
                else
                {
                    //frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor
                    robotDrive =
                            new RobotDrive(PWMChannels.LEFT_DRIVE_MOTOR_A,
                                           PWMChannels.LEFT_DRIVE_MOTOR_B,
                                           PWMChannels.RIGHT_DRIVE_MOTOR_A,
                                           PWMChannels.RIGHT_DRIVE_MOTOR_B);
                    robotDrive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, invertLeftSideA);
                    robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, invertLeftSideB);
                    robotDrive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, invertRightSideA);
                    robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, invertRightSideB);
                }

            }
            else
            {
                LOG.info("DriveTrainSubsystem is disabled. Will not initialize it.");
            }

        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.initRobotDrive()", e);
        }
    }


    public void initDefaultCommand()
    {
        setDefaultCommand(new DriveCommand());
    }


    /**
     * Toggles the flag that indicates if the drive should be set to decrease the joystick sensitivity at lower speeds. When this flag is true, it decreases the sensitivity at
     * lower speeds
     *
     * @return the new value of the <tt>decreaseSensitivityAtLowerSpeeds</tt> flag
     */
    public boolean toggleDecreaseSensitivityAtLowerSpeeds()
    {
        decreaseSensitivityAtLowerSpeeds = !decreaseSensitivityAtLowerSpeeds;
        updateSensitivityModeDisplay();
        return decreaseSensitivityAtLowerSpeeds;
    }


    /**
     * Drives the robot via either Tank Drive or Arcade Drive depending on the current setting of the
     * {@link #setControlMode(ControlMode)} flag/switch.
     * Example use when the 'left' joystick is used as the stick to use in arcade mode:
     * <pre>
     *     Joystick leftStick = ...
     *     Joystick rightStick = ...
     *     . . .
     *     driveTrainSubSystem.drive(leftStick, rightStick, leftStick);
     * </pre>
     *
     * @param leftStick          The joystick used to control the left side in tank drive mode
     * @param rightStick         The joystick used to control the right side in tank drive mode
     * @param stickUsedForArcade The joystick used to control the drive train in arcade mode. This may
     *                           or may not be the same instance/object as either of the right
     *                           or left joysticks
     */
    public void drive(GenericHID leftStick, GenericHID rightStick, GenericHID stickUsedForArcade)
    {
        if (controlMode.equals(ControlMode.ARCADE))
        {
            driveViaArcadeDrive(stickUsedForArcade);
        }
        else if (controlMode.equals(ControlMode.TANK))
        {
            driveViaTankDrive(leftStick, rightStick);
        }
        else
        {
            throw new IllegalArgumentException("Unknown ControlMode of '" + controlMode + "' in drive() method");
        }
    }


    /**
     * Provide tank steering using the stored robot configuration. drive the robot using two joystick inputs. The Y-axis will be selected from each Joystick object.
     *
     * @param leftStick  The joystick to control the left side of the robot.
     * @param rightStick The joystick to control the right side of the robot.
     */
    public void driveViaTankDrive(GenericHID leftStick, GenericHID rightStick)
    {
        try
        {
            if (isEnabled())
            {
                if (leftStick == null || rightStick == null)
                {
                    LOG.error("A joystick is null in DriveTrainSubsystem.driveViaTankDrive()");
                }
                else
                {
                    double leftY = leftStick.getY();
                    double rightY = rightStick.getY();
                    if (Math.abs(leftY) < deadZoneThreshold) {leftY = 0;}
                    if (Math.abs(rightY) < deadZoneThreshold) {rightY = 0;}
                    robotDrive.tankDrive(leftY, rightY, decreaseSensitivityAtLowerSpeeds);
                    updateDistanceDisplay();
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.driveViaTankDrive()", e);
        }
    }


    /**
     * Drives the DriveTrainSubsystem using arcade drive.
     *
     * @param stick The joystick to use for Arcade single-stick driving. The Y-axis will be selected for forwards/backwards and the X-axis will be selected for rotation rate.
     *
     * @see RobotDrive#arcadeDrive(GenericHID)
     */
    public void driveViaArcadeDrive(GenericHID stick)
    {
        try
        {
            if (isEnabled())
            {
                if (stick == null)
                {
                    LOG.error("The passed in joystick is null in DriveTrainSubsystem.driveViaArcadeDrive()");
                }
                else
                {
                    double x = stick.getX();
                    double y = stick.getY();

                    if (Math.abs(x) < deadZoneThreshold)
                    {
                        x = 0;
                    }

                    if (Math.abs(y) < deadZoneThreshold)
                    {
                        y = 0;
                    }

                    robotDrive.arcadeDrive(y, x, decreaseSensitivityAtLowerSpeeds);
                    updateDistanceDisplay();
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.driveViaArcadeDrive()", e);
        }
    }


    /**
     * Sets the flag that indicates if the drive should be set to decrease the joystick sensitivity at lower speeds. Setting this flag to true decreases the sensitivity at lower
     * speeds.
     *
     * @param value the new flag value
     */
    public void setDecreaseSensitivityAtLowerSpeeds(boolean value)
    {
        DriveTrainSubsystem.decreaseSensitivityAtLowerSpeeds = value;
        updateSensitivityModeDisplay();
    }


    private void updateSensitivityModeDisplay()
    {
        SmartDashboard.putString(SDKeys.DRIVE_SENSITIVITY, getSensitivityModeStatus());
    }


    private void updateControlModeDisplay()
    {
        SmartDashboard.putString(SDKeys.DRIVE_CONTROL_MODE, getControlMode().getName());
    }


    public void updateDistanceDisplay()
    {
        try
        {
            if (EM.isDriveTrainSubsystemReportDistanceEnabled)
            {
                SmartDashboard.putData(SDKeys.LEFT_ENCODER, leftEncoder);
                SmartDashboard.putData(SDKeys.RIGHT_ENCODER, rightEncoder);


                SmartDashboard.putNumber(SDKeys.LEFT_ENCODER_DISTANCE_TRAVELED, leftEncoder.getDistance());
                SmartDashboard.putNumber(SDKeys.RIGHT_ENCODER_DISTANCE_TRAVELED, rightEncoder.getDistance());

                SmartDashboard.putNumber(SDKeys.LEFT_ENCODER_COUNT, leftEncoder.get());
                SmartDashboard.putNumber(SDKeys.RIGHT_ENCODER_COUNT, rightEncoder.get());

                SmartDashboard.putString(SDKeys.LEFT_ENCODER_STATUS, createEncoderStatusString(leftEncoder));
                SmartDashboard.putString(SDKeys.RIGHT_ENCODER_STATUS, createEncoderStatusString(rightEncoder));
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.DriveTrainSubsystem.updateDistanceDisplay()", e);
        }

    }


    private String createEncoderStatusString(Encoder encoder)
    {
        return "dir: " + encoder.getDirection() + " rate: " + encoder.getRate() + "   raw: " + encoder.getRaw()
                + " samplesToAvg: " + encoder.getSamplesToAverage() + " count: " + encoder.get() + " distance: " + encoder.getDistance();
    }


    private String getSensitivityModeStatus()
    {
        return decreaseSensitivityAtLowerSpeeds ? "Low Sens" : "Standard";
    }


    public boolean isEnabled()
    {
        return EM.isDriveTrainSubsystemEnabled;
    }


    public ControlMode getControlMode()
    {
        return controlMode;
    }


    public void setControlMode(ControlMode controlMode)
    {
        DriveTrainSubsystem.controlMode = controlMode;
        updateControlModeDisplay();
    }


    public void toggleControlMode()
    {
        if (controlMode.equals(ControlMode.ARCADE))
        { setControlMode(ControlMode.TANK); }
        else if (controlMode.equals(ControlMode.TANK))
        { setControlMode(controlMode = ControlMode.ARCADE); }
        else
        {throw new IllegalStateException("Unknown Control Mode in 'toggleControlMode()'. Current Mode is: " + getControlMode());}
    }


    public void resetEncoders()
    {
        try
        {
            if (EM.isDriveTrainSubsystemEncodersEnabled)
            {
                leftEncoder.reset();
                rightEncoder.reset();
                updateDistanceDisplay();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.resetEncoders()", e);
        }

    }


    /** @noinspection UseOfObsoleteCollectionType, UnusedDeclaration */
    public static class ControlMode
    {
        private String name;
        public final int ordinal;
        private ControlMode previous;
        private ControlMode next;
        private static ControlMode first = null;
        private static ControlMode last = null;
        private static int upperBound = 0;
        private static Hashtable values = new Hashtable();
        private static Hashtable ordinalLookup = new Hashtable();

        public static final ControlMode TANK = new ControlMode("TANK");
        public static final ControlMode ARCADE = new ControlMode("ARCADE");

        //Lazy initialized collection objects
        private static Vector vector;
        private static ControlMode[] array;
        private static String[] nameArray;


        /**
         * Constructs a new ControlMode enumeration instance (i.e. constant). This constructor is private so that outside class cannot add any enumeration instances.
         *
         * @param name the name of the enumeration instance (i.e. constant), typically in all uppercase per standard naming conventions
         */
        private ControlMode(String name)
        {
            this.name = name;
            ordinal = upperBound++;
            if (first == null) first = this;
            if (last != null)
            {
                previous = last;
                last.next = this;
            }
            last = this;
            values.put(name, this);
            ordinalLookup.put(Integer.valueOf(ordinal), this);
        }


        /**
         * Returns an {@link Enumeration} of all {@code ControlMode}s.
         *
         * @return an Enumeration of all {@code ControlMode}s
         */
        public static Enumeration getEnumeration()
        {
            return new Enumeration()
            {
                private ControlMode current = first;


                public boolean hasMoreElements()
                {
                    return current != null;
                }


                public Object nextElement()
                {
                    if (current == null)
                    {
                        throw new NoSuchElementException("There are no more elements in the Enumeration");
                    }
                    ControlMode theNextElement = current;
                    current = current.next();
                    return theNextElement;
                }
            };
        }


        public static Vector asVector()
        {
            if (vector == null)
            {
                vector = new Vector(size());
                Enumeration enumeration = getEnumeration();
                while (enumeration.hasMoreElements())
                {
                    ControlMode item = (ControlMode) enumeration.nextElement();
                    vector.addElement(item);
                }
            }
            return vector;
        }


        /**
         * Returns an array of all {@code ControlMode}s.
         *
         * @return an array of all {@code ControlMode}s
         */
        public static ControlMode[] asArray()
        {
            if (array == null)
            {
                array = new ControlMode[size()];
                Enumeration enumeration = getEnumeration();
                int index = 0;
                while (enumeration.hasMoreElements())
                {
                    ControlMode item = (ControlMode) enumeration.nextElement();
                    array[index++] = item;
                }
            }
            return array;
        }


        /**
         * An array of the names of all {@code ControlMode}s.
         *
         * @return an array of the names of all {@code ControlMode}s.
         */
        public static String[] asArrayOfNames()
        {
            if (nameArray == null)
            {
                nameArray = new String[size()];
                Enumeration enumeration = getEnumeration();
                int index = 0;
                while (enumeration.hasMoreElements())
                {
                    ControlMode item = (ControlMode) enumeration.nextElement();
                    nameArray[index++] = item.getName();
                }
            }
            return nameArray;
        }


        /**
         * Indicates whether some other object is &quot;equal to&quot; this one. <p> The <code>equals</code> method implements an equivalence relation on non-null
         * object references: <ul> <li>It is <i>reflexive</i>: for any non-null reference value <code>x</code>, <code>x.equals(x)</code> should return
         * {@code true}. <li>It is <i>symmetric</i>: for any non-null reference values <code>x</code> and <code>y</code>, <code>x.equals(y)</code> should
         * return {@code true} if and only if <code>y.equals(x)</code> returns {@code true}. <li>It is <i>transitive</i>: for any non-null reference
         * values <code>x</code>, <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code> returns {@code true} and <code>y.equals(z)</code> returns
         * {@code true}, then <code>x.equals(z)</code> should return {@code true}. <li>It is <i>consistent</i>: for any non-null reference values
         * <code>x</code> and <code>y</code>, multiple invocations of {@code x.equals(y)} consistently return {@code true} or consistently return
         * {@code false}, provided no information used in <code>equals</code> comparisons on the objects is modified. <li>For any non-null reference value
         * <code>x</code>, <code>x.equals(null)</code> should return {@code false}. </ul>
         *
         * @param o the reference object with which to compare.
         *
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        public boolean equals(Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }
            ControlMode castedObject = (ControlMode) o;
            return name.equals(castedObject.name);
        }


        /**
         * An overloading of the standard {@link #equals(Object)} method that takes a {@code String} representing
         * the name of an enumerated instance. Use of this method is semantically equivalent to:
         * <pre>
         * ControlMode.FOO.equals(ControlMode.valueOf(myLoggingLevel);
         * </pre>
         * and
         * <pre>
         * ControlMode.FOO.getName().equals(myLoggingLevel.getName());
         * </pre>
         * It is provided as a convenience method for situation where the caller is dealing with a String that maps to a {@code ControlMode}.
         *
         * @param name the name of the enumeration instance (i.e. constant) with which to compare
         *
         * @return {@code true} if this object is the same name as the name argument; {@code false} otherwise.
         */


        public boolean equals(String name)
        { return this.name.equals(name); }


        /**
         * An overloading of the standard {@link #equals(Object)} method that takes an {@code int} representing the ordinal
         * of an enumerated instance. Use of this method is semantically equivalent to:
         * <pre>
         * ControlMode.FOO.ordinal == myLoggingLevel.ordinal;
         * </pre>
         *
         * @param ordinal the ordinal value with which to compare to
         *
         * @return{@code true} if this object's ordinal is the same as the ordinal argument; {@code false} otherwise.
         */
        public boolean equals(int ordinal)
        { return this.ordinal == ordinal; }


        public int hashCode() { return name.hashCode(); }


        /**
         * Returns the name of the enumeration instance (i.e. constant). For {@code ControlMode.FOO} this method would return &quot;FOO&quot;.
         *
         * @return the name of the enumeration instance (i.e. constant)
         */
        public String getName()
        { return this.name; }


        /**
         * The size of the enumeration set. In other words, how may enumeration instances (i.e. constants) exist.
         *
         * @return size of the enumeration set
         */
        public static int size()
        { return values.size(); }


        /**
         * Gets the first <tt>ControlMode<tt> in the enumeration set.
         *
         * @return the first <tt>ControlMode<tt> in the enumeration set
         */
        public static ControlMode first()
        { return first; }


        /**
         * Gets the last <tt>ControlMode<tt> in the enumeration set.
         *
         * @return the last <tt>ControlMode<tt> in the enumeration set.
         */
        public static ControlMode last()
        { return last; }


        /**
         * Gets a {@code ControlMode} represented name. May return {@code null} if the name received does not represent a valid enumeration instance.
         *
         * @param name the name to of the {@code ControlMode} to get
         *
         * @return the {@code ControlMode} represented by the name argument, or {@code null} if there is no {@code ControlMode} represented by the received name
         */
        public static ControlMode valueOf(String name)
        { return (ControlMode) values.get(name); }


        /**
         * Gets a {@code ControlMode} represented by an ordinal. May return {@code null} if the ordinal received does not represent a valid enumeration instance.
         *
         * @param ordinal the ordinal to of the {@code ControlMode} to get
         *
         * @return the {@code ControlMode} represented by the ordinal argument, or {@code null} if there is no {@code ControlMode} represented by the received ordinal
         */
        public static ControlMode valueOf(int ordinal)
        { return (ControlMode) ordinalLookup.get(Integer.valueOf(ordinal)); }


        /**
         * Gets the previous {@code ControlMode} in the enumeration. If this instance is the first {@code ControlMode}, {@code null} is returned.
         *
         * @return the previous {@code ControlMode} or {@code null} if this is the first {@code ControlMode} in the enumeration
         */
        public ControlMode previous()
        { return this.previous; }


        /**
         * Gets the next {@code ControlMode} in the enumeration. If this instance is the last {@code ControlMode}, {@code null} is returned.
         *
         * @return the next {@code ControlMode} or {@code null} if this is the last {@code ControlMode} in the enumeration
         */
        public ControlMode next()
        { return this.next; }


        /**
         * Returns the name of the enumeration instance (i.e. constant). For {@code ControlMode.FOO} this method would return &quot;FOO&quot;.
         *
         * @return the name of the enumeration instance (i.e. constant)
         */
        public String toString()
        { return this.name; }


        /**
         * Gets the ordinal of the enumeration instance (i.e. constant). This is a convenience method since the {@link #ordinal} property itself is public.
         *
         * @return the ordinal of the enumeration instance (i.e. constant)
         */
        public int getOrdinal()
        { return ordinal; }
    }
}
