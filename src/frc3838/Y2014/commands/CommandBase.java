package frc3838.Y2014.commands;

import edu.wpi.first.wpilibj.SensorBase;
import edu.wpi.first.wpilibj.command.Command;
import frc3838.Y2014.OI2014;
import frc3838.Y2014.controls.Buttons;
import frc3838.Y2014.subsystems.CompressorSubsystem;
import frc3838.Y2014.subsystems.DriveTrainSubsystem;
import frc3838.Y2014.subsystems.LedRingLightSubsystem;
import frc3838.Y2014.subsystems.ShooterSubsystem;
import frc3838.Y2014.subsystems.TurretDirectSubsystem;
import frc3838.Y2014.subsystems.TurretSubsystem;
import frc3838.Y2014.subsystems.VacSubsystem;
import frc3838.Y2014.subsystems.flipper.FlipperSubsystem;
import frc3838.Y2014.utils.LOG;



/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 *
 * @author Author
 */
public abstract class CommandBase extends Command
{
    public static OI2014 oi2014Instance;
    // Create a single *static* instance of all of your subsystems
    protected static final DriveTrainSubsystem driveTrainSubsystem = DriveTrainSubsystem.getInstance();
    protected static final CompressorSubsystem compressorSubsystem = CompressorSubsystem.getInstance();
    protected static final TurretSubsystem turretSubsystem = TurretSubsystem.getInstance();
    protected static final TurretDirectSubsystem turretDirectSubsystem = TurretDirectSubsystem.getInstance();
    protected static final FlipperSubsystem rightFlipperSubsystem = FlipperSubsystem.getRightInstance();
    protected static final FlipperSubsystem leftFlipperSubsystem = FlipperSubsystem.getLeftInstance();
    protected static final VacSubsystem vacSubsystem = VacSubsystem.getInstance();
    protected static final ShooterSubsystem shooterSubsystem = ShooterSubsystem.getInstance();
    protected static final LedRingLightSubsystem ledRingLightSubsystem = LedRingLightSubsystem.getInstance();


    /** @noinspection ConstructorNotProtectedInAbstractClass */
    public CommandBase(String name)
    {
        super(name);
    }


    /** @noinspection ConstructorNotProtectedInAbstractClass */
    public CommandBase()
    {
        super();
    }


    public static void init()
    {
        LOG.debug("Entering CommandBase.init()");
        LOG.info("Initializing CommandBase (i.e. CommandBase.init()");

        SensorBase.setDefaultDigitalModule(1);
        /*

            Typically, OI2014 should be the very first thing initialized

         */

        if (oi2014Instance == null)
        {
            LOG.debug("Calling OI2014 constructor");
            // This MUST be here. If the OI2014 creates Commands (which it very likely
            // will), constructing it during the construction of CommandBase (from
            // which commands extend), subsystems are not guaranteed to be
            // yet. Thus, their requires() statements may grab null pointers. Bad
            // news. Don't move it.
            try
            {
                oi2014Instance = new OI2014();
            }
            catch (Exception e)
            {
                LOG.error(">>>FATAL<<< Could not construct the OI2014 instance in CommandBase.init(): " + e.toString(), e);
            }
            LOG.trace("Returned from OI2014 Constructor call");
        }
        else
        {
            LOG.info("oi2014Instance was already created. Was CommandBase.init() called multiple times????");
        }

        //ensure Buttons class has been initialized
        LOG.debug("Number of configured/assigned buttons: " + Buttons.getNumberOfAssignedButtons());

        // Show what command your subsystem is running on the SmartDashboard
        //SmartDashboard.putData(exampleSubsystem);


        // **NOTE: We create our buttons in the Buttons class.

        LOG.trace("Exiting CommandBase.init()");
    }
}
