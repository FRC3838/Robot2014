package frc3838.Y2014.controls;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;

import edu.wpi.first.wpilibj.Joystick;
import frc3838.Y2014.utils.LOG;



/** @noinspection UseOfObsoleteCollectionType, UnusedDeclaration */
public class JoystickDef
{
    private String name;
    public final int ordinal;
    private JoystickDef previous;
    private JoystickDef next;

    private final Joystick joystick;
    /** Trigger button. */
    private final NamedJoystickButton button1;
    /** Bottom high hat thumb button. * */
    private final NamedJoystickButton button2;
    /** Top high hat thumb button. */
    private final NamedJoystickButton button3;
    /** Left high hat thumb button. */
    private final NamedJoystickButton button4;
    /** Right high hat thumb button. */
    private final NamedJoystickButton button5;
    /** Left group upper/front button. */
    private final NamedJoystickButton button6;
    /** Left group lower/rear button. */
    private final NamedJoystickButton button7;
    /** Lower group left button. */
    private final NamedJoystickButton button8;
    /** Lower group right button. */
    private final NamedJoystickButton button9;
    /** Right group lower button. */
    private final NamedJoystickButton button10;
    /** Right group upper/front button. */
    private final NamedJoystickButton button11;

    private final NamedJoystickButton[] buttons;

    private static JoystickDef first = null;
    private static JoystickDef last = null;
    private static int upperBound = 0;
    private static Hashtable values = new Hashtable();
    private static Hashtable ordinalLookup = new Hashtable();
    private static Hashtable allButtonsLookup = new Hashtable(44);


    // LIST JOYSTICK IN PORT ORDER FROM 1 to 4
    /* 1 */public static final JoystickDef driverLeft = new JoystickDef("driverLeft");
    /* 2 */public static final JoystickDef driverRight = new JoystickDef("driverRight");
    /* 3 */public static final JoystickDef opsLeft = new JoystickDef("opsLeft");
    /* 4 */public static final JoystickDef opsRight = new JoystickDef("opsRight");

    //Lazy initialized collection objects
    private static Vector vector;
    private static JoystickDef[] array;
    private static String[] nameArray;


    public static void init()
    {
        final Vector asVector = asVector();
    }


    public static JoystickDef getArcadeDriveAssignedJoystickDef()
    {
        return driverRight;
    }
    public static JoystickDef getDriverNonArcadeJoystickDef() { return driverLeft; }


    /**
     * Constructs a new JoystickDef enumeration instance (i.e. constant). This constructor is private so that outside class cannot add any enumeration instances.
     *
     * @param name the name of the enumeration instance (i.e. constant), typically in all uppercase per standard naming conventions
     */
    private JoystickDef(String name)
    {
        this.name = name;
        ordinal = upperBound++;
        if (first == null) first = this;
        if (last != null)
        {
            previous = last;
            last.next = this;
        }
        last = this;
        values.put(name, this);
        ordinalLookup.put(Integer.valueOf(ordinal), this);
        int port = ordinal + 1;
        joystick = new Joystick(port);
        button1 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(1));
        button2 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(2));
        button3 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(3));
        button4 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(4));
        button5 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(5));
        button6 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(6));
        button7 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(7));
        button8 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(8));
        button9 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(9));
        button10 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(10));
        button11 = new NamedJoystickButton(joystick, name, port, NamedJoystickButton.ButtonName.valueOfButtonNumber(11));
        buttons = new NamedJoystickButton[] {button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11};
    }


    /**
     * Returns an {@link Enumeration} of all {@code JoystickDef}s.
     *
     * @return an Enumeration of all {@code JoystickDef}s
     */
    public static Enumeration getEnumeration()
    {
        return new Enumeration()
        {
            private JoystickDef current = first;


            public boolean hasMoreElements()
            {
                return current != null;
            }


            public Object nextElement()
            {
                if (current == null)
                {
                    throw new NoSuchElementException("There are no more elements in the Enumeration");
                }
                JoystickDef theNextElement = current;
                current = current.next();
                return theNextElement;
            }
        };
    }


    public static Vector asVector()
    {
        if (vector == null)
        {
            vector = new Vector(size());
            Enumeration enumeration = getEnumeration();
            while (enumeration.hasMoreElements())
            {
                JoystickDef item = (JoystickDef) enumeration.nextElement();
                vector.addElement(item);
            }
        }
        return vector;
    }


    /**
     * Returns an array of all {@code JoystickDef}s.
     *
     * @return an array of all {@code JoystickDef}s
     */
    public static JoystickDef[] asArray()
    {
        if (array == null)
        {
            array = new JoystickDef[size()];
            Enumeration enumeration = getEnumeration();
            int index = 0;
            while (enumeration.hasMoreElements())
            {
                JoystickDef item = (JoystickDef) enumeration.nextElement();
                array[index++] = item;
            }
        }
        return array;
    }


    /**
     * An array of the names of all {@code JoystickDef}s.
     *
     * @return an array of the names of all {@code JoystickDef}s.
     */
    public static String[] asArrayOfNames()
    {
        if (nameArray == null)
        {
            nameArray = new String[size()];
            Enumeration enumeration = getEnumeration();
            int index = 0;
            while (enumeration.hasMoreElements())
            {
                JoystickDef item = (JoystickDef) enumeration.nextElement();
                nameArray[index++] = item.getName();
            }
        }
        return nameArray;
    }


    /**
     * Indicates whether some other object is &quot;equal to&quot; this one. <p> The <code>equals</code> method implements an equivalence relation on non-null
     * object references: <ul> <li>It is <i>reflexive</i>: for any non-null reference value <code>x</code>, <code>x.equals(x)</code> should return
     * {@code true}. <li>It is <i>symmetric</i>: for any non-null reference values <code>x</code> and <code>y</code>, <code>x.equals(y)</code> should
     * return {@code true} if and only if <code>y.equals(x)</code> returns {@code true}. <li>It is <i>transitive</i>: for any non-null reference
     * values <code>x</code>, <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code> returns {@code true} and <code>y.equals(z)</code> returns
     * {@code true}, then <code>x.equals(z)</code> should return {@code true}. <li>It is <i>consistent</i>: for any non-null reference values
     * <code>x</code> and <code>y</code>, multiple invocations of {@code x.equals(y)} consistently return {@code true} or consistently return
     * {@code false}, provided no information used in <code>equals</code> comparisons on the objects is modified. <li>For any non-null reference value
     * <code>x</code>, <code>x.equals(null)</code> should return {@code false}. </ul>
     *
     * @param o the reference object with which to compare.
     *
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        JoystickDef castedObject = (JoystickDef) o;
        return name.equals(castedObject.name);
    }


    /**
     * An overloading of the standard {@link #equals(Object)} method that takes a {@code String} representing
     * the name of an enumerated instance. Use of this method is semantically equivalent to:
     * <pre>
     * JoystickDef.FOO.equals(JoystickDef.valueOf(myLoggingLevel);
     * </pre>
     * and
     * <pre>
     * JoystickDef.FOO.getName().equals(myLoggingLevel.getName());
     * </pre>
     * It is provided as a convenience method for situation where the caller is dealing with a String that maps to a {@code JoystickDef}.
     *
     * @param name the name of the enumeration instance (i.e. constant) with which to compare
     *
     * @return {@code true} if this object is the same name as the name argument; {@code false} otherwise.
     */


    public boolean equals(String name)
    { return this.name.equals(name); }


    /**
     * An overloading of the standard {@link #equals(Object)} method that takes an {@code int} representing the ordinal
     * of an enumerated instance. Use of this method is semantically equivalent to:
     * <pre>
     * JoystickDef.FOO.ordinal == myLoggingLevel.ordinal;
     * </pre>
     *
     * @param ordinal the ordinal value with which to compare to
     *
     * @return {@code true} if this object's ordinal is the same as the ordinal argument; {@code false} otherwise.
     */
    public boolean equals(int ordinal)
    { return this.ordinal == ordinal; }


    public int hashCode() { return name.hashCode(); }


    /**
     * Returns the name of the enumeration instance (i.e. constant). For {@code JoystickDef.FOO} this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String getName()
    { return this.name; }


    /**
     * The size of the enumeration set. In other words, how may enumeration instances (i.e. constants) exist.
     *
     * @return size of the enumeration set
     */
    public static int size()
    { return values.size(); }


    /**
     * Gets the first <tt>JoystickDef<tt> in the enumeration set.
     *
     * @return the first <tt>JoystickDef<tt> in the enumeration set
     */
    public static JoystickDef first()
    { return first; }


    /**
     * Gets the last <tt>JoystickDef<tt> in the enumeration set.
     *
     * @return the last <tt>JoystickDef<tt> in the enumeration set.
     */
    public static JoystickDef last()
    { return last; }


    /**
     * Gets a {@code JoystickDef} represented name. May return {@code null} if the name received does not represent a valid enumeration instance.
     *
     * @param name the name to of the {@code JoystickDef} to get
     *
     * @return the {@code JoystickDef} represented by the name argument, or {@code null} if there is no {@code JoystickDef} represented by the received name
     */
    public static JoystickDef valueOf(String name)
    { return (JoystickDef) values.get(name); }


    /**
     * Gets a {@code JoystickDef} represented by an ordinal. May return {@code null} if the ordinal received does not represent a valid enumeration instance.
     *
     * @param ordinal the ordinal to of the {@code JoystickDef} to get
     *
     * @return the {@code JoystickDef} represented by the ordinal argument, or {@code null} if there is no {@code JoystickDef} represented by the received ordinal
     */
    public static JoystickDef valueOf(int ordinal)
    { return (JoystickDef) ordinalLookup.get(Integer.valueOf(ordinal)); }


    /**
     * Gets the previous {@code JoystickDef} in the enumeration. If this instance is the first {@code JoystickDef}, {@code null} is returned.
     *
     * @return the previous {@code JoystickDef} or {@code null} if this is the first {@code JoystickDef} in the enumeration
     */
    public JoystickDef previous()
    { return this.previous; }


    /**
     * Gets the next {@code JoystickDef} in the enumeration. If this instance is the last {@code JoystickDef}, {@code null} is returned.
     *
     * @return the next {@code JoystickDef} or {@code null} if this is the last {@code JoystickDef} in the enumeration
     */
    public JoystickDef next()
    { return this.next; }


    /**
     * Returns the name of the enumeration instance (i.e. constant). For {@code JoystickDef.FOO} this method would return &quot;FOO&quot;.
     *
     * @return the name of the enumeration instance (i.e. constant)
     */
    public String toString()
    { return this.name; }


    /**
     * Gets the ordinal of the enumeration instance (i.e. constant). This is a convenience method since the {@link #ordinal} property itself is public.
     *
     * @return the ordinal of the enumeration instance (i.e. constant)
     */
    public int getOrdinal()
    { return ordinal; }


    public int getPort()
    {
        return ordinal + 1;
    }


    public Joystick getJoystick()
    {
        return joystick;
    }


    /**
     * Returns a joystick button by its number (1-11).
     *
     * <pre>
     *        JOYSTICK BUTTON LAYOUT
     * ***************************************
     * *             (1 Trigger)             *
     * *          .................          *
     * *          :               :          *
     * * +---+    :     +---+     :    +---+ *
     * * | 6 |    . +-+ | 3 | +-+ :    + 11| *
     * * +---+    : |4| +---+ |5| :    +---+ *
     * *          : +-+ +---+ +-+ :          *
     * * +---+    :     | 2 |     :    +---+ *
     * * | 7 |    :     +---+     :    | 10| *
     * * +---+    :...............:    +---+ *
     * * Left          Stick           Right *
     * * Group      +---+  +---+       Group *
     * *            | 8 |  | 9 |             *
     * *            +---+  +---+             *
     * *            Bottom Group             *
     * ***************************************
     * </pre>
     *
     * @param buttonNumber the button number to get. Must be between 1 and 11 inclusive.
     *
     * @return the joystick button for the button number
     *
     * @throws IllegalArgumentException if the @code buttonNumber} is out of the allowable range of 1-11.
     */
    public NamedJoystickButton getButton(int buttonNumber) throws IllegalArgumentException
    {
        if (buttonNumber < 1 || buttonNumber > 11)
        {
            String message = buttonNumber + " is not a valid Joystick button number. Must be between 1-11";
            LOG.error(message);
            throw new IllegalArgumentException(message);
        }
        return buttons[buttonNumber - 1];
    }


    public NamedJoystickButton getButton(NamedJoystickButton.ButtonName buttonName)
    {
        return buttons[buttonName.getButtonNumber() - 1];
    }


    /** Trigger button - #1. */
    public NamedJoystickButton getTriggerButton1()
    { return button1; }


    /** Bottom high hat thumb button - #2. * */
    public NamedJoystickButton getHighHatBottomButton2()
    { return button2; }


    /** Top high hat thumb button - #3. */
    public NamedJoystickButton getHighHatTopButton3()
    { return button3; }


    /** Left high hat thumb button - #4. */
    public NamedJoystickButton getHighHatLeftButton4()
    { return button4; }


    /** Right high hat thumb button - #5. */
    public NamedJoystickButton getHighHatRightButton5()
    { return button5; }


    /** Left group upper/front button - #6. */
    public NamedJoystickButton getLeftGroupUpperButton6() { return button6; }


    /** Left group lower/rear button - #7. */
    public NamedJoystickButton getLeftGroupLowerButton7()
    { return button7; }


    /** Lower group left button - #8. */
    public NamedJoystickButton getLowerGroupLeftButton8()
    { return button8; }


    /** Lower group right button - #9. */
    public NamedJoystickButton getLowerGroupRightButton9()
    { return button9; }


    /** Right group lower/rear button - #10. */
    public NamedJoystickButton getRightGroupLowerButton10()
    { return button10; }


    /** Right group upper/front button - #11. */
    public NamedJoystickButton getRightGroupUpperBottomButton11()
    { return button11; }


}
