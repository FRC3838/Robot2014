package frc3838.Y2014.commands.vac;


public class VacCommandsFactory
{
    private static final StartVacCommand startVacCommand;
    private static final StopVacCommand stopVacCommand;
    
    static
    {
        startVacCommand = new StartVacCommand();
        stopVacCommand = new StopVacCommand();
        startVacCommand.setStopVacCommand(stopVacCommand);
        stopVacCommand.setStartVacCommand(startVacCommand);
    }


    public static StartVacCommand getStartVacCommand()
    {
        return startVacCommand;
    }


    public static StopVacCommand getStopVacCommand()
    {
        return stopVacCommand;
    }
}
