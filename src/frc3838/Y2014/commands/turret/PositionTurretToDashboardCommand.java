package frc3838.Y2014.commands.turret;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.controls.SDKeys;



public class PositionTurretToDashboardCommand extends AbstractPositionTurretCommand
{


    protected double getTargetPosition()
    {
        return SmartDashboard.getNumber(SDKeys.TURRET_POSITION_TARGET);
    }
}
