package frc3838.Y2014.subsystems.motors;


import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.livewindow.LiveWindowSendable;
import edu.wpi.first.wpilibj.tables.ITable;
import frc3838.Y2014.utils.LOG;
import frc3838.Y2014.utils.math.Math2;



/** @noinspection UnusedDeclaration */ /* @noinspection UnusedDeclaration */
public class MotorOps implements PIDOutput,
                                 LiveWindowSendable

{
    // Motor settings
    //    setRaw() ranges from 0 to 255. 128 to 255 is forward speed, and 127 to 0 is reversed
    //    set() ranges from -1 to 1 and is provided for victor.

    private static final double STOP = 0;
    public static final double DEFAULT_MIN_SPEED = 0.01;

    private boolean inverse;
    private int inverseFactor;

    private double minNonStopSpeed = DEFAULT_MIN_SPEED;

    /** The speed <b>without</b> alteration for forward or reversed. */
    private double speed;

    private SpeedController controller;
    private LiveWindowSendable liveWindowSendable;


    /**
     * @param speedController the {@link SpeedController} (CANJaguar, Jaguar, Talon or Victor) to control
     * @param inverse         if the motor should be inverted via software
     */
    public MotorOps(SpeedController speedController, boolean inverse)
    {
        this(speedController, inverse, DEFAULT_MIN_SPEED);
    }


    /**
     * @param speedController the {@link SpeedController} (CANJaguar, Jaguar, Talon or Victor) to control
     * @param inverse         if the motor should be inverted via software
     * @param minNonStopSpeed the minimum speed (to prevent motor stalling or burn out)
     */
    public MotorOps(SpeedController speedController, boolean inverse, double minNonStopSpeed)
    {
        if (!(speedController instanceof LiveWindowSendable))
        {
            throw new IllegalArgumentException("Supplied SpeedController does not implement LiveWindowSendable interface... it needs to.");
        }
        this.controller = speedController;
        this.liveWindowSendable = (LiveWindowSendable) controller;
        this.inverse = inverse;
        inverseFactor = inverse ? -1 : 1;
        this.minNonStopSpeed = minNonStopSpeed;
        speedController.set(STOP);
    }
   

    public LiveWindowSendable getControllerAsLiveWindowSendable()
    {
        return liveWindowSendable;
    }


    /** @param speed the speed (between -1 and 1) such that a positive speed is forward, a negative speed is reverse, and zero is stopped. */
    public String setSpeed(double speed)
    {
        this.speed = constrainValue(speed);
        return setSpeedToCurrentSetting();
    }


    /**
     * Sets the speed without modifying the run state such that if the motor is running, its speed with change, but if
     * the motor is stopped, the speed is set for a call to one of the startAtCurrentSpeed methods.
     * @param speed the speed to use
     */
    public void setSpeedWithoutModifyingRunState(double speed)
    {
        this.speed = constrainValue(speed);
        if (isRunning())
        {
            setSpeedToCurrentSetting();
        }
    }


    public void setMinNonStopSpeed(double minNonStopSpeed)
    {
        this.minNonStopSpeed = constrainValue(minNonStopSpeed);
    }


    protected String setSpeedToCurrentSetting()
    {
        if (Math.abs(speed) < minNonStopSpeed)
        {
            speed = 0;
        }
        speed = constrainValue(speed);
        controller.set(speed * inverseFactor);
        return getSpeedPercentage();
    }


    /**
     * Makes use of a PID Controller's output by calling the {@link edu.wpi.first.wpilibj.SpeedController#pidWrite(double) pidWrite(double)} method on the SpeedController.
     *
     * @param pidOutput the
     */
    public void pidWrite(double pidOutput)
    {
        controller.pidWrite(pidOutput);
        speed = controller.get();
    }
    public double startAtCurrentSetSpeedAndDirection()
    {
        setSpeedToCurrentSetting();
        return speed;
    }
    
    public double startForwardAtCurrentRelativeSpeed()
    {
        double neededSpeed = Math.abs(speed);
        setSpeed(neededSpeed);
        return this.speed;
    }
    
    
    public double startReverseAtCurrentRelativeSpeed()
    {
        double neededSpeed = - Math.abs(speed);
        setSpeed(neededSpeed);
        return this.speed;
    }
    
    
    public double toggleDirection()
    {
        this.speed = -this.speed;
        setSpeedToCurrentSetting();
        return this.speed;
    }

    public String stop()
    {
        //We do not set the speed field to zero as we want to retain the last speed used for a restart.
        controller.set(STOP);
        return getSpeedPercentage();
    }


    public void setForward10Percent() { setSpeed(0.1);}


    public void setForward25Percent() { setSpeed(0.25); }


    public void setForward50Percent() { setSpeed(0.5); }


    public void setForward75Percent() { setSpeed(0.75); }


    public void setForwardAsPercent(int percentage)
    {
        if (percentage < 0)
        {
            LOG.error("Received forward speed is less than 0%. Setting to 0&");
            percentage = 0;
        }
        else if (percentage > 100)
        {
            LOG.error("Received forward speed is greater than 100%. Setting to 100%");
            percentage = 100;
        }
        double targetSpeed = percentage / 100;
        setSpeed(targetSpeed);
    }


    public void setForwardFull() { setSpeed(1.0); }


    public void setReverse10Percent() { setSpeed(-0.1); }


    public void setReverse25Percent() { setSpeed(-0.25); }


    public void setReverse50Percent() { setSpeed(-0.5); }


    public void setReverse75Percent() { setSpeed(-0.75); }


    public void setReverseAsPercent(int percentage)
    {
        percentage = Math.abs(percentage);
        if (percentage < 0)
        {
            LOG.error("Received reverse speed is less than 0%. Setting to 0%");
            percentage = 0;
        }
        else if (percentage > 100)
        {
            LOG.error("Received revers speed is greater than 100%. Setting to 100%");
            percentage = 100;
        }
        double targetSpeed = percentage / 100;
        setSpeed(-targetSpeed);
    }


    public void setReverseFull() { setSpeed(-1.0); }


    public String increaseForwardSpeedOnePercent() { return increaseForwardSpeed(.01); }


    public String increaseForwardSpeedThreePercent() { return increaseForwardSpeed(.03); }


    public String increaseForwardSpeedFivePercent() { return increaseForwardSpeed(.05); }


    public String increaseForwardSpeedTenPercent() { return increaseForwardSpeed(.1); }


    public String decreaseForwardSpeedOnePercent() { return decreaseForwardSpeed(.01); }


    public String decreaseForwardSpeedThreePercent() { return decreaseForwardSpeed(.03); }


    public String decreaseForwardSpeedFivePercent() { return decreaseForwardSpeed(.05); }


    public String decreaseForwardSpeedTenPercent() { return decreaseForwardSpeed(.1); }


    /**
     * Increases the forward speed by the specified amount, but does nto allow the motor to go into reverse. For example, if the current speed is 5% (i.e. 0.05) forward and a value
     * of change of 8% (i.e. 0.08) is passed in, the motor speed will be set to 0, not 3% reversed.
     *
     * @param delta the amount to decrease as a value between 0 and 1.
     *
     * @return the new speed
     */
    public String decreaseForwardSpeed(double delta)
    { return modifySpeed(-Math.abs(delta)); }


    public String increaseForwardSpeed(double delta) { return modifySpeed(Math.abs(delta));}


    public boolean isRunning()
    {
        return controller.get() != 0;
    }

    public boolean isRunningFullSpeed()
    {
        return Math.abs(controller.get()) >= 1.0D;
    }

    private String modifySpeed(double delta)
    {
        double realDelta = delta * inverseFactor;
        double current = controller.get();
        double newSpeed = current + realDelta;

        speed = constrainValue(newSpeed);
        return setSpeedToCurrentSetting();
    }

    public double getSpeed()
    {
        return controller.get();
    }
    
    public double getAbsoluteSpeed()
    {
        return Math.abs(controller.get());
    }

    /**
     * Constrains the the speed to between -1 and 1. Note that this method does <b>not</b> set the speed field or modify the motor's speed. It is simply a math helper method.
     *
     * @param speed the speed to constrain
     *
     * @return the constrained speed
     */
    public double constrainValue(double speed)
    {
        if (speed > 1)
        {
            return 1;
        }
        else if (speed < -1)
        {
            return -1;
        }
        else
        {
            return speed;
        }
    }


    public String getSpeedPercentage()
    {
        if (controller.get() == 0)
        {
            return "STOPPED";
        }
        return isForward() ? "F:" + Math2.toPercentage(controller.get()) : "R:" + Math2.toPercentage(controller.get());
    }


    public boolean isForward()
    {

        return inverse ? controller.get() < 0 : controller.get() > 0;
    }


    public void updateTable() { getControllerAsLiveWindowSendable().updateTable(); }


    public void startLiveWindowMode() { getControllerAsLiveWindowSendable().startLiveWindowMode(); }


    public void stopLiveWindowMode() { getControllerAsLiveWindowSendable().stopLiveWindowMode(); }


    public void initTable(ITable subtable) { getControllerAsLiveWindowSendable().initTable(subtable); }


    public ITable getTable() { return getControllerAsLiveWindowSendable().getTable(); }


    public String getSmartDashboardType() { return getControllerAsLiveWindowSendable().getSmartDashboardType(); }
}
