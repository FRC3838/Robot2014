package frc3838.Y2014.controls;


/** @noinspection PointlessBooleanExpression */
public final class EM
{
    public static boolean isCompressorSubsystemEnabled = true;

    public static boolean isDriveTrainSubsystemEnabled = true;
    public static boolean isDriveTrainSubsystemEncodersEnabled = (isDriveTrainSubsystemEnabled && true);
    public static boolean isDriveTrainSubsystemReportDistanceEnabled = (isDriveTrainSubsystemEncodersEnabled && true);
    
    
    public static boolean isTurretDirectSubsystemEnabled = false;
    public static boolean isTurretSubsystemEnabled = true;

    public static boolean isVacSubsystemEnabled = true;
    public static boolean isShooterSubsystemEnabled = true;

    public static boolean isFlippersSubsystemEnabled = true;
    public static boolean isTestBot = false;
    public static boolean isLedRingLightSubsystemEnabled = true;


    private EM() {}
}
