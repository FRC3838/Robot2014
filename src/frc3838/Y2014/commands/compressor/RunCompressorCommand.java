package frc3838.Y2014.commands.compressor;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class RunCompressorCommand extends CommandBase
{
    /** @noinspection FieldCanBeLocal */
    private static final boolean DEBUG_COMPRESSOR = false;


    public RunCompressorCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(compressorSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            compressorSubsystem.startCompressor();
        }
        else
        {
            LOG.info("Not all required subsystems for RunCompressorCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                //We do not need to do anything repeatedly for the compressor. This command simply needs to initialize, and then end
                if (DEBUG_COMPRESSOR)
                {
                    LOG.debug("Compressor Digital Switch Value: " + compressorSubsystem.getPressureSwitchValue());
                }
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in RunCompressorCommand.execute()", e);
            }

        }
        else
        {
            LOG.trace("Not all required subsystems for RunCompressorCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (compressorSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
        LOG.trace("RunCompressorCommand end() method called. Stopping compressor.");
        //noinspection ProhibitedExceptionCaught
        try
        {
            compressorSubsystem.stopCompressor();
        }
        catch (NullPointerException e)
        {
            if (!allSubsystemsAreEnabled())
            {
                LOG.error("A NullPointerException occurred when attempting to stop the compressor even though all required subsystems are enabled.", e);
            }
        }
        catch (Exception e)
        {
            LOG.error("An Exception occurred when attempting to stop the compressor.", e);
        }
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
