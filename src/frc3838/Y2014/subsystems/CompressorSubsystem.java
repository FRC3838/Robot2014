package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2014.commands.compressor.RunCompressorCommand;
import frc3838.Y2014.controls.DIO;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.Relays;
import frc3838.Y2014.utils.LOG;



public class CompressorSubsystem extends Subsystem
{

    private static final CompressorSubsystem singleton = new CompressorSubsystem();
    private static Compressor compressor;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new CompressorSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     CompressorSubsystem subsystem = new CompressorSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     CompressorSubsystem subsystem = CompressorSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static CompressorSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     CompressorSubsystem subsystem = new CompressorSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     CompressorSubsystem subsystem = CompressorSubsystem.getInstance();
     * </pre>
     */
    private CompressorSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in CompressorSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing CompressorSubsystem");
                compressor = new Compressor(DIO.COMPRESSOR_PRESSURE_SWITCH_CHANNEL,
                                            Relays.COMPRESSOR_RELAY_CHANNEL);
                LOG.debug("CompressorSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                EM.isCompressorSubsystemEnabled = false;
                LOG.error("An exception occurred in CompressorSubsystem.init()", e);
            }

        }
        else
        {
            LOG.info("CompressorSubsystem is disabled and will not be initialized");
        }
    }


    // Put methods for controlling this subsystem here,
    // Call these from Commands.
    // Be sure to wrap in an if(isSubsystemEnabled) block


    public void startCompressor()
    {
        try
        {
            if (isEnabled() && compressor != null)
            {
                compressor.start();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in CompressorSubsystem.startCompressor()", e);
        }
    }


    public void startCompressorAndAutoRun()
    {
        setDefaultCommand(new RunCompressorCommand());
    }


    public void stopCompressor()
    {
        try
        {
            if (isEnabled() && compressor != null)
            {
                compressor.stop();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in CompressorSubsystem.stopCompressor()", e);
        }
    }


    public boolean getPressureSwitchValue()
    {
        return compressor != null && compressor.getPressureSwitchValue();
    }


    public void initDefaultCommand()
    {
    }


    public boolean isEnabled()
    {
        return EM.isCompressorSubsystemEnabled;
    }
}
