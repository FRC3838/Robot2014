package frc3838.Y2014.commands.turret;


public class PositionTurretTo120Command extends AbstractPositionTurretCommand
{
    
    
    protected double getTargetPosition()
    {
        return 120.0;
    }
}
