package frc3838.Y2014.commands.drive;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.controls.JoystickDef;
import frc3838.Y2014.utils.LOG;



public class DriveCommand extends CommandBase
{
    
    private static boolean disabledMessageLogged = false;
    public DriveCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(driveTrainSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (!allSubsystemsAreEnabled() && !disabledMessageLogged)
        {
            LOG.info("Not all required subsystems for DriveCommand are enabled. The command can not be and will not be initialized or executed.");
            disabledMessageLogged = true;
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                //Third argument is joystick used for arcade drive mode
                driveTrainSubsystem.drive(JoystickDef.driverLeft.getJoystick(),
                                          JoystickDef.driverRight.getJoystick(),
                                          JoystickDef.getArcadeDriveAssignedJoystickDef().getJoystick());
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in DriveCommand.execute()", e);
            }
        }
        
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (driveTrainSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
        //No op
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
        //No op
    }
}
