package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.Relays;
import frc3838.Y2014.utils.LOG;



public class ShooterSubsystem extends Subsystem
{

    private static final ShooterSubsystem singleton = new ShooterSubsystem();
//    private Solenoid solenoid;
    private Relay solenoidRelay;
    
    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use
     * of a constructor such as <tt>new ShooterSubsystem()</tt> in order to use ensure
     * only a single instance of a Subsystem is created. This is known as a Singleton.
     * For example, instead of doing this:
     * <pre>
     *     ShooterSubsystem subsystem = new ShooterSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     ShooterSubsystem subsystem = ShooterSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static ShooterSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ShooterSubsystem subsystem = new ShooterSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     ShooterSubsystem subsystem = ShooterSubsystem.getInstance();
     * </pre>
     */
    private ShooterSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in ShooterSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing ShooterSubsystem");
                
//                solenoid = new Solenoid(Relays.FIRING_PIN_SOLENOID);
                solenoidRelay = new Relay(Relays.FIRING_PIN_SOLENOID);
                solenoidRelay.setDirection(Relay.Direction.kForward);
                retract();
                
                LOG.debug("ShooterSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                EM.isShooterSubsystemEnabled = false;
                LOG.error("An exception occurred in ShooterSubsystem.init()", e);
            }
        }
        else
        {
            LOG.info("ShooterSubsystem is disabled and will not be initialized");
        }
    }
    
//    public void fire() { solenoid.set(false); }
//    public void retract() {solenoid.set(true);}

    public void fire() { solenoidRelay.set(Relay.Value.kOff); }
    public void retract() { solenoidRelay.set(Relay.Value.kOn);}


    public void initDefaultCommand()
    {
        // no op - no default command needed at this time
    }


    public boolean isEnabled()
    {
        return EM.isShooterSubsystemEnabled;
    }
}
