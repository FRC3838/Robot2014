package frc3838.Y2014.subsystems;


import edu.wpi.first.wpilibj.AnalogPotentiometer;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.commands.turretDirect.UpdateTurretDirectSpeedCommand;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.SDKeys;
import frc3838.Y2014.subsystems.motors.MotorOps;
import frc3838.Y2014.utils.LOG;



/**
 * Subsystem extension of the WPI Lib {@link PIDSubsystem} for controlling a subsystem that uses
 * a single <a href="http://en.wikipedia.org/wiki/PID_controller">proportional-integral-derivative controller (PID controller)</a>
 * (see {@link edu.wpi.first.wpilibj.PIDController}) almost constantly (for instance, an elevator which attempts to stay
 * at a constant height).
 *
 * <p>It provides some convenience methods to run an internal {@link edu.wpi.first.wpilibj.PIDController}.
 * It also allows access to the internal {@link edu.wpi.first.wpilibj.PIDController} in order to give total control
 * to the programmer.</p>
 */
public class TurretDirectSubsystem extends Subsystem
{

    private static final TurretDirectSubsystem singleton = new TurretDirectSubsystem();

    public static final double DEFAULT_MOTOR_SPEED = 0.355;
    public static final double STALL_SPEED = 0.16;
    /** @noinspection FieldCanBeLocal */
    private double motorSpeed = DEFAULT_MOTOR_SPEED;

    private double potMinValue = Double.MAX_VALUE;
    private double potMaxValue = Double.MIN_VALUE;

    private AnalogPotentiometer potentiometer;


    private static final boolean inverseMotor = true;
    private MotorOps motorOps;
    private SpeedController speedController;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such
     * as <tt>new TurretDirectSubsystem()</tt> in order to use ensure only a single instance of a Subsystem is created. This
     * is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     TurretDirectSubsystem subsystem = new TurretDirectSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     TurretDirectSubsystem subsystem = TurretDirectSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static TurretDirectSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     TurretDirectSubsystem subsystem = new TurretDirectSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     TurretDirectSubsystem subsystem = TurretDirectSubsystem.getInstance();
     * </pre>
     */
    private TurretDirectSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in TurretDirectSubsystem() constructor", e);
        }

    }


    private void init()
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing TurretDirectSubsystem");

                SmartDashboard.putBoolean(SDKeys.PID_TWEAKER_ENABLED, true);

                potentiometer = TurretComponents.getInstance().getPotentiometer();

                speedController = TurretComponents.getInstance().getSpeedController();
                motorOps = new MotorOps(speedController, inverseMotor, STALL_SPEED);
            }
            catch (Exception e)
            {
                EM.isTurretDirectSubsystemEnabled = false;
                LOG.error("An exception occurred in TurretDirectSubsystem.init()", e);
            }
        }
        else
        {
            LOG.info("TurretDirectSubsystem is disabled and will not be initialized");
        }
    }

    public void startMotorInForward()
    {
        try
        {
            LOG.debug("TurretDirectSubsystem.startMotorInForward() called");
            if (isEnabled())
            {
                if (motorOps != null)
                {
                    motorSpeed = readSpeedSetting();
                    LOG.debug("Starting turret motor at a speed of " + motorSpeed);
                    motorOps.setSpeed(motorSpeed);
                }

                updateMotorSpeedDisplay();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.startMotorInForward()", e);
        }

    }

    public void restartMotor()
    {
        try
        {
            LOG.debug("TurretDirectSubsystem.restartMotor() called");
            if (isEnabled())
            {
                if (motorOps != null)
                {
                    updateMotorSpeedFromDashboard();
                    motorOps.setSpeed(motorSpeed);
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.subsystems.TurretDirectSubsystem.restartMotor()", e);
        }
    }


    public void stopMotor()
    {
        try
        {
            LOG.debug("TurretDirectSubsystem.stopMotor)_ called");
            if (isEnabled())
            {
                if (motorOps != null)
                {
                    motorOps.stop();
                }
                updateMotorSpeedDisplay();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.stopMotor()", e);
        }
    }


    public void startMotorInReverse()
    {
        try
        {
            LOG.debug("TurretDirectSubsystem.startMotorInReverse() called");
            if (isEnabled())
            {
                if (motorOps != null)
                {
                    motorSpeed = -readSpeedSetting();
                    LOG.debug("Starting turret motor (in reverse) at a speed of " + -motorSpeed);
                    motorOps.setSpeed(motorSpeed);
                }

                updateMotorSpeedDisplay();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.startMotorInReverse()", e);
        }
    }


    public void toggleMotor()
    {
        try
        {
            LOG.debug("TurretDirectSubsystem.toggleMotor() called");
            if (isEnabled())
            {
                if (motorOps.isRunning())
                {
                    stopMotor();
                }
                else
                {
                    restartMotor();
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TurretDirectSubsystem.toggleMotor()", e);
        }
    }


    public double updateMotorSpeedFromDashboard()
    {
        final double speedSetting = readSpeedSetting();
//        if (speedSetting != Math.abs(motorSpeed))
        {
            motorSpeed = isMotorSetToReverse() ? -speedSetting : motorSpeed;
            LOG.debug("Updating motor speed to " + motorSpeed);
            if (motorOps.isRunning())
            {
                motorOps.setSpeed(motorSpeed);
            }
        }
        updateDisplay();
        return motorSpeed;
    }

    public void initDefaultCommand()
    {
        setDefaultCommand(new UpdateTurretDirectSpeedCommand());
    }



    public void updateDisplay()
    {
        updateMotorSpeedDisplay();
        updatePotentiometerDisplay();
    }

    public void updatePotentiometerDisplay()
    {
        if (isEnabled() && potentiometer != null)
        {
            SmartDashboard.putNumber(SDKeys.TURRET_POT_RAW_VALUE, getPotentiometerRawValue());
            SmartDashboard.putString(SDKeys.TURRET_POT_RAW_STRING_VALUE, Double.toString(getPotentiometerRawValue()));

            SmartDashboard.putNumber(SDKeys.TURRET_POT_PID_VALUE, getPotentiometerPidValue());
            SmartDashboard.putString(SDKeys.TURRET_POT_PID_STRING_VALUE, Double.toString(getPotentiometerPidValue()));
        }
    }

    public double getPotentiometerRawValue()
    {
        return potentiometer.get();
    }


    public double getPotentiometerPidValue()
    {
        return potentiometer.pidGet();
    }


    public double readSpeedSetting()
    {
        try
        {
            //noinspection UnnecessaryLocalVariable
            double motorSpeed = SmartDashboard.getNumber(SDKeys.TURRET_MOTOR_SPEED_CONTROL, DEFAULT_MOTOR_SPEED);
            return motorSpeed;
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TurretDirectSubsystem.readSpeedSetting()", e);
            return DEFAULT_MOTOR_SPEED;
        }
    }


    public void updateMotorSpeedDisplay()
    {
        try
        {
            if (isEnabled())
            {
                if (motorOps != null)
                {
                    SmartDashboard.putString(SDKeys.TURRET_MOTOR_STATUS, motorOps.getSpeedPercentage());
                }
                else
                {
                    SmartDashboard.putString(SDKeys.TURRET_MOTOR_STATUS, "isNull");
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TurretDirectSubsystem.updateMotorSpeedDisplay()", e);
        }

    }


    public boolean isMotorRunning()
    {
        try
        {
            return motorOps.isRunning();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in TurretDirectSubsystem.isMotorRunning()", e);
        }
        return true;
    }


    public boolean isMotorSetToReverse()
    {
        return !(motorSpeed == Math.abs(motorSpeed));
    }

    public boolean isEnabled()
    {
        return EM.isTurretDirectSubsystemEnabled;
    }
}