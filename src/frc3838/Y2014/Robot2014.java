/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc3838.Y2014;


import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.commands.autonomous.AutonomousCommandGroup;
import frc3838.Y2014.controls.EM;
import frc3838.Y2014.controls.SDKeys;
import frc3838.Y2014.subsystems.CompressorSubsystem;
import frc3838.Y2014.subsystems.DriveTrainSubsystem;
import frc3838.Y2014.utils.LOG;
import frc3838.Y2014.utils.LogLevel;



/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot2014 extends IterativeRobot
{

    public static final LogLevel LOG_LEVEL = LogLevel.INFO;
    public static final boolean IN_DEBUG_MODE = true;

    /** Gets set in the {@link #testInit()} method is applicable. */
    private static boolean inTestMode = false;


    private boolean bootCompleteMsgHasBeenLogged = false;
    private Command autonomousCommand;
    private SendableChooser autonomousChooser;


    // static initializer block - For info on such, see http://docs.oracle.com/javase/tutorial/java/javaOO/initial.html
    static
    {
        //noinspection UseOfSystemOutOrSystemErr
        System.out.println("Robot2012 --> RobotMap.LOG_LEVEL = " + LOG_LEVEL);
    }


    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     *
     * @noinspection RefusedBequest
     */
    public void robotInit()
    {
        try
        {
            //noinspection UseOfSystemOutOrSystemErr
            System.out.println("Robot2012 int() --> RobotMap.LOG_LEVEL = " + LOG_LEVEL);
            LOG.debug("Robot2014.robotInit() called");
            bootCompleteMsgHasBeenLogged = false;

            // Initialize all subsystems
            CommandBase.init();

            autonomousChooser = new SendableChooser();
            autonomousChooser.addDefault("Default Autonomous Mode", new AutonomousCommandGroup());
            SmartDashboard.putData(SDKeys.AUTONOMOUS_MODE_CHOOSER, autonomousChooser);
            resetDriveTrainEncoders();

        }
        catch (RuntimeException e)
        {
            final String message = ">>>FATAL<<< An exception occurred in the Robot2014.robotInit() method: " + e.toString();
            LOG.error(message, e);
            throw e;
        }
    }


    private void resetDriveTrainEncoders() 
    {
        try
        {
            DriveTrainSubsystem.getInstance().resetEncoders();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in frc3838.Y2014.Robot2014.resetDriveTrainEncoders()", e);
        }

    }


    /** @noinspection RefusedBequest */
    public void autonomousInit()
    {
        if (EM.isCompressorSubsystemEnabled)
        {
            try { CompressorSubsystem.getInstance().startCompressorAndAutoRun(); }
            catch (Exception e) { LOG.error("An exception occurred in autonomousInit() when starting compressor", e); }
        }
        resetDriveTrainEncoders();

        if (autonomousCommand != null)
        {
            try
            {
                // instantiate the command used for the autonomous period
                // autonomousCommand = new AutonomousCommandGroup();

                autonomousCommand = (Command) autonomousChooser.getSelected();
                if (autonomousChooser == null)
                {
                    LOG.error("Autonomous command selection was NOT read from SmartDashboard");
                }
                else
                {
                    LOG.info("Running " + autonomousCommand.getName() + " autonomous command as selected on SmartDashboard");
                }

            }
            catch (Exception e)
            {
                final String message = ">>>MAJOR<<< Could not construct the autonomous command due to an exception: " + e.toString();
                LOG.error(message, e);
            }
        }

        if (autonomousCommand != null)
        {
            try
            {
                // schedule the autonomous command (example)
                autonomousCommand.start();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when executing autonomousCommand.start() in Robot2014.autonomousInit(): " + e.toString(), e);
            }
        }
        else
        {
            LOG.error("autonomousCommand is null. Cannot execute autonomousCommand.start() in Robot2014.autonomousInit()");
        }
    }


    /**
     * This function is called periodically during autonomous
     *
     * @noinspection RefusedBequest
     */
    public void autonomousPeriodic()
    {
        try
        {
            Scheduler.getInstance().run();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in Robot2014.autonomousPeriodic()", e);
        }
    }


    /** @noinspection RefusedBequest */
    public void teleopInit()
    {
        resetDriveTrainEncoders();
        if (autonomousCommand != null)
        {
            try
            {
                // This makes sure that the autonomous stops running when
                // teleop starts running. If you want the autonomous to
                // continue until interrupted by another command, remove
                // this line or comment it out.
                autonomousCommand.cancel();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred when executing autonomousCommand.cancel() in Robot2014.teleopInit(): " + e.toString(), e);
            }
        }
        else
        {
            LOG.error("autonomousCommand is null. Cannot execute autonomousCommand.cancel() in Robot2014.teleopInit()");
        }


        if (EM.isCompressorSubsystemEnabled)
        {
            try { CompressorSubsystem.getInstance().startCompressorAndAutoRun(); }
            catch (Exception e) { LOG.error("An exception occurred in Robot2014.teleopInit() when starting the compressor", e); }
        }
    }


    /**
     * This function is called periodically during operator control
     *
     * @noinspection RefusedBequest
     */
    public void teleopPeriodic()
    {
        if (EM.isCompressorSubsystemEnabled)
        {
            try { CompressorSubsystem.getInstance().startCompressorAndAutoRun(); }
            catch (Exception e) { LOG.error("An exception occurred in Robot2014.teleopInit() when starting the compressor", e); }
        }
        try
        {
            Scheduler.getInstance().run();
            
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in Robot2014.teleopPeriodic()", e);
        }
        
    }


    /**
     * This function is called periodically during test mode
     *
     * @noinspection RefusedBequest
     */
    public void testPeriodic()
    {
        LOG.trace("TEST MODE:: Robot2014.testPeriodic() method called");
        LiveWindow.run();
    }


    /**
     * Initialization code for test mode should go here.
     *
     * Users should override this method for initialization code which will be called each time
     * the robot enters test mode.
     * @noinspection RefusedBequest
     */
    public void testInit()
    {
        LOG.info("TEST MODE:: Robot2014.testInit() called");
        inTestMode = true;
        LiveWindow.setEnabled(true);
    }


    /** @noinspection RefusedBequest */
    public void disabledInit()
    {
        LOG.info("Robot entering disabled mode");

        if (!bootCompleteMsgHasBeenLogged)
        {
            bootCompleteMsgHasBeenLogged = true;
            LOG.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            LOG.info("~ ROBOT BOOT UP COMPLETED ~");
            LOG.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }

        try
        {
            if (EM.isCompressorSubsystemEnabled)
            {
                CompressorSubsystem.getInstance().stopCompressor();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in Robot2014.disabledInit() when stopping the compressor", e);
        }


    }


    /** @noinspection RefusedBequest */
    public void disabledPeriodic()
    {
        //no op at this time
        //this method is periodically called when the robot is in disabled mode
    }


    public static boolean isInTestMode() { return inTestMode; }
}
