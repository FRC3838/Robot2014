package frc3838.Y2014.commands.shooter;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class RetractCommand extends CommandBase
{
    public RetractCommand()
    {
        requires(shooterSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        //noinspection StatementWithEmptyBody
        if (allSubsystemsAreEnabled())
        {
            // No op
        }
        else
        {
            LOG.info("Not all required subsystems for RetractCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                shooterSubsystem.retract();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in RetractCommand.execute()", e);
            }
        }
        else
        {
            LOG.trace("Not all required subsystems for RetractCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (shooterSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
