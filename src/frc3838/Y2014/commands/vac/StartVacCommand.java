package frc3838.Y2014.commands.vac;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class StartVacCommand extends CommandBase
{
    private StopVacCommand stopVacCommand;


    /**
     * Use {@link VacCommandsFactory} to get an instance.
     */
    StartVacCommand()
    {
        requires(vacSubsystem);
    }


    void setStopVacCommand(StopVacCommand stopVacCommand)
    {
        this.stopVacCommand = stopVacCommand;
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            if (stopVacCommand.isRunning())
            {
                stopVacCommand.cancel();
            }
        }
        else
        {
            LOG.info("Not all required subsystems for StartVacCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                vacSubsystem.getMotorOps().increaseForwardSpeedTenPercent();
                
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in StartVacCommand.execute()", e);
            }
        }
        else
        {
            LOG.trace("Not all required subsystems for StartVacCommand are enabled. The command can not be and will not be executed.");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return vacSubsystem.getMotorOps().isRunningFullSpeed();
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (vacSubsystem.isEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
