package frc3838.Y2014.commands.turret;

import frc3838.Y2014.commands.CommandBase;
import frc3838.Y2014.utils.LOG;



public class StopAndDisableTurretEnabledCommand extends CommandBase
{

    public StopAndDisableTurretEnabledCommand()
    {
        // Use requires() here to declare subsystem dependencies

        requires(turretSubsystem);
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        if (allSubsystemsAreEnabled())
        {
            try
            {
                turretSubsystem.stopAndDisable();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in StopAndDisableTurretEnabledCommand.initialize() when trying to stop the turret", e);
            }
        }
        else
        {
            LOG.info("Not all required subsystems for StopAndDisableTurretEnabledCommand are enabled. The command can not be and will not be initialized or executed.");
        }
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        //Stopping the turret is so critical, we do not check if the system is enabled. Better to get a NullPointer than to not be able to send the stop command
        try
        {
            turretSubsystem.stopAndDisable();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in StopAndDisableTurretEnabledCommand.execute() when trying to stop the turret", e);
        }

    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    protected boolean allSubsystemsAreEnabled()
    {
        return (turretSubsystem.isSubsystemEnabled());
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
        //We redundantly call the stop command in the end method to absolutely ensure the turret is stopped - overkill, but we want to be absolutely safe
        try
        {
            turretSubsystem.stopAndDisable();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in StopAndDisableTurretEnabledCommand.end() when trying to stop the turret", e);
        }
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
        try
        {
            turretSubsystem.stopAndDisable();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in StopAndDisableTurretEnabledCommand.interrupted() when trying to stop the turret", e);
        }
    }
}
