package frc3838.Y2014.controls;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.JoystickButton;



public class NamedJoystickButton extends JoystickButton
{
    private final ButtonName buttonName;

    private final GenericHID joystick;
    private final String joystickName;
    private final int joystickPort;


    /**
     * Create a joystick button for triggering commands
     *
     * @param joystick     The GenericHID object that has the button (e.g. Joystick, KinectStick, etc)
     * @param joystickName The name of the joystick (used in identifying information such as toString, equals, and hashCode)
     * @param joystickPort The port of the joystick (used in identifying information such as toString, equals, and hashCode)
     * @param buttonName   The button name
     */
    NamedJoystickButton(GenericHID joystick, String joystickName, int joystickPort, ButtonName buttonName)
    {
        super(joystick, buttonName.getButtonNumber());
        this.buttonName = buttonName;
        this.joystick = joystick;
        this.joystickPort = joystickPort;
        this.joystickName = joystickName;
    }


    public ButtonName getButtonName()
    {
        return buttonName;
    }


    /** @noinspection RedundantIfStatement, QuestionableName, MethodWithMultipleReturnPoints, OverlyComplexMethod */
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NamedJoystickButton that = (NamedJoystickButton) o;

        if (joystickPort != that.joystickPort) return false;
        if (!buttonName.equals(that.buttonName)) return false;
        if (!joystick.equals(that.joystick)) return false;
        if (!joystickName.equals(that.joystickName)) return false;

        return true;
    }


    public int hashCode()
    {
        int result = buttonName.hashCode();
        result = 31 * result + joystick.hashCode();
        result = 31 * result + joystickName.hashCode();
        result = 31 * result + joystickPort;
        return result;
    }


    public String toString()
    {
        return buttonName + " on " + joystickName + " (port #" + joystickPort + ')';
    }


    /** @noinspection UseOfObsoleteCollectionType, UnusedDeclaration */
    public static class ButtonName
    {
        private String name;
        public final int ordinal;
        private ButtonName previous;
        private ButtonName next;
        private static ButtonName first = null;
        private static ButtonName last = null;
        private static int upperBound = 0;
        private static Hashtable values = new Hashtable();
        private static Hashtable ordinalLookup = new Hashtable();

        /*  1 */ public static final ButtonName trigger = new ButtonName("trigger");
        /*  2 */ public static final ButtonName highHatBottom = new ButtonName("highHatBottom");
        /*  3 */ public static final ButtonName highHatTop = new ButtonName("highHatTop");
        /*  4 */ public static final ButtonName highHatLeft = new ButtonName("highHatLeft");
        /*  5 */ public static final ButtonName highHatRight = new ButtonName("highHatRight");
        /*  6 */ public static final ButtonName leftGroupUpper = new ButtonName("leftGroupUpper");
        /*  7 */ public static final ButtonName leftGroupLower = new ButtonName("leftGroupLower");
        /*  8 */ public static final ButtonName bottomGroupLeft = new ButtonName("bottomGroupLeft");
        /*  9 */ public static final ButtonName bottomGroupRight = new ButtonName("bottomGroupRight");
        /* 10 */ public static final ButtonName rightGroupLower = new ButtonName("rightGroupLower");
        /* 11 */ public static final ButtonName rightGroupUpper = new ButtonName("rightGroupUpper");

        //Lazy initialized collection objects
        private static Vector vector;
        private static ButtonName[] array;
        private static String[] nameArray;


        /**
         * Constructs a new ButtonName enumeration instance (i.e. constant). This constructor is private so that outside class cannot add any enumeration instances.
         *
         * @param name the name of the enumeration instance (i.e. constant), typically in all uppercase per standard naming conventions
         */
        private ButtonName(String name)
        {
            this.name = name;
            ordinal = upperBound++;
            if (first == null) first = this;
            if (last != null)
            {
                previous = last;
                last.next = this;
            }
            last = this;
            values.put(name, this);
            ordinalLookup.put(Integer.valueOf(ordinal), this);
        }


        /**
         * Returns an {@link Enumeration} of all {@code ButtonName}s.
         *
         * @return an Enumeration of all {@code ButtonName}s
         */
        public static Enumeration getEnumeration()
        {
            return new Enumeration()
            {
                private ButtonName current = first;


                public boolean hasMoreElements()
                {
                    return current != null;
                }


                public Object nextElement()
                {
                    if (current == null)
                    {
                        throw new NoSuchElementException("There are no more elements in the Enumeration");
                    }
                    ButtonName theNextElement = current;
                    current = current.next();
                    return theNextElement;
                }
            };
        }


        public static Vector asVector()
        {
            if (vector == null)
            {
                vector = new Vector(size());
                Enumeration enumeration = getEnumeration();
                while (enumeration.hasMoreElements())
                {
                    ButtonName item = (ButtonName) enumeration.nextElement();
                    vector.addElement(item);
                }
            }
            return vector;
        }


        /**
         * Returns an array of all {@code ButtonName}s.
         *
         * @return an array of all {@code ButtonName}s
         */
        public static ButtonName[] asArray()
        {
            if (array == null)
            {
                array = new ButtonName[size()];
                Enumeration enumeration = getEnumeration();
                int index = 0;
                while (enumeration.hasMoreElements())
                {
                    ButtonName item = (ButtonName) enumeration.nextElement();
                    array[index++] = item;
                }
            }
            return array;
        }


        /**
         * An array of the names of all {@code ButtonName}s.
         *
         * @return an array of the names of all {@code ButtonName}s.
         */
        public static String[] asArrayOfNames()
        {
            if (nameArray == null)
            {
                nameArray = new String[size()];
                Enumeration enumeration = getEnumeration();
                int index = 0;
                while (enumeration.hasMoreElements())
                {
                    ButtonName item = (ButtonName) enumeration.nextElement();
                    nameArray[index++] = item.getName();
                }
            }
            return nameArray;
        }


        /**
         * Indicates whether some other object is &quot;equal to&quot; this one. <p> The <code>equals</code> method implements an equivalence relation on non-null
         * object references: <ul> <li>It is <i>reflexive</i>: for any non-null reference value <code>x</code>, <code>x.equals(x)</code> should return
         * {@code true}. <li>It is <i>symmetric</i>: for any non-null reference values <code>x</code> and <code>y</code>, <code>x.equals(y)</code> should
         * return {@code true} if and only if <code>y.equals(x)</code> returns {@code true}. <li>It is <i>transitive</i>: for any non-null reference
         * values <code>x</code>, <code>y</code>, and <code>z</code>, if <code>x.equals(y)</code> returns {@code true} and <code>y.equals(z)</code> returns
         * {@code true}, then <code>x.equals(z)</code> should return {@code true}. <li>It is <i>consistent</i>: for any non-null reference values
         * <code>x</code> and <code>y</code>, multiple invocations of {@code x.equals(y)} consistently return {@code true} or consistently return
         * {@code false}, provided no information used in <code>equals</code> comparisons on the objects is modified. <li>For any non-null reference value
         * <code>x</code>, <code>x.equals(null)</code> should return {@code false}. </ul>
         *
         * @param o the reference object with which to compare.
         *
         * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
         */
        public boolean equals(Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }
            ButtonName castedObject = (ButtonName) o;
            return name.equals(castedObject.name);
        }


        /**
         * An overloading of the standard {@link #equals(Object)} method that takes a {@code String} representing
         * the name of an enumerated instance. Use of this method is semantically equivalent to:
         * <pre>
         * ButtonName.FOO.equals(ButtonName.valueOf(myLoggingLevel);
         * </pre>
         * and
         * <pre>
         * ButtonName.FOO.getName().equals(myLoggingLevel.getName());
         * </pre>
         * It is provided as a convenience method for situation where the caller is dealing with a String that maps to a {@code ButtonName}.
         *
         * @param name the name of the enumeration instance (i.e. constant) with which to compare
         *
         * @return {@code true} if this object is the same name as the name argument; {@code false} otherwise.
         */
        public boolean equals(String name)
        { return this.name.equals(name); }


        public int hashCode()
        {
            int result = name.hashCode();
            result = 31 * result + ordinal;
            result = 31 * result + previous.hashCode();
            result = 31 * result + next.hashCode();
            return result;
        }


        /**
         * Returns the name of the enumeration instance (i.e. constant). For {@code ButtonName.FOO} this method would return &quot;FOO&quot;.
         *
         * @return the name of the enumeration instance (i.e. constant)
         */
        public String getName()
        { return this.name; }


        /**
         * The size of the enumeration set. In other words, how may enumeration instances (i.e. constants) exist.
         *
         * @return size of the enumeration set
         */
        public static int size()
        { return values.size(); }


        /**
         * Gets the first <tt>ButtonName<tt> in the enumeration set.
         *
         * @return the first <tt>ButtonName<tt> in the enumeration set
         */
        public static ButtonName first()
        { return first; }


        /**
         * Gets the last <tt>ButtonName<tt> in the enumeration set.
         *
         * @return the last <tt>ButtonName<tt> in the enumeration set.
         */
        public static ButtonName last()
        { return last; }


        /**
         * Gets a {@code ButtonName} represented name. May return {@code null} if the name received does not represent a valid enumeration instance.
         *
         * @param name the name to of the {@code ButtonName} to get
         *
         * @return the {@code ButtonName} represented by the name argument, or {@code null} if there is no {@code ButtonName} represented by the received name
         */
        public static ButtonName valueOf(String name)
        { return (ButtonName) values.get(name); }


        /**
         * Gets a {@code ButtonName} represented by an ordinal. May return {@code null} if the ordinal received does not represent a valid enumeration instance.
         *
         * @param ordinal the ordinal to of the {@code ButtonName} to get
         *
         * @return the {@code ButtonName} represented by the ordinal argument, or {@code null} if there is no {@code ButtonName} represented by the received ordinal
         */
        public static ButtonName valueOf(int ordinal)
        { return (ButtonName) ordinalLookup.get(Integer.valueOf(ordinal)); }


        /**
         * Gets a {@code ButtonName} represented by a buttonNumber. May return {@code null} if the buttonNumber received does not represent a valid enumeration instance.
         *
         * @param buttonNumber the buttonNumber to of the {@code ButtonName} to get
         *
         * @return the {@code ButtonName} represented by the buttonNumber argument, or {@code null} if there is no {@code ButtonName} represented by the received buttonNumber
         */
        public static ButtonName valueOfButtonNumber(int buttonNumber)
        { return (ButtonName) ordinalLookup.get(Integer.valueOf(buttonNumber - 1)); }


        /**
         * Gets the previous {@code ButtonName} in the enumeration. If this instance is the first {@code ButtonName}, {@code null} is returned.
         *
         * @return the previous {@code ButtonName} or {@code null} if this is the first {@code ButtonName} in the enumeration
         */
        public ButtonName previous()
        { return this.previous; }


        /**
         * Gets the next {@code ButtonName} in the enumeration. If this instance is the last {@code ButtonName}, {@code null} is returned.
         *
         * @return the next {@code ButtonName} or {@code null} if this is the last {@code ButtonName} in the enumeration
         */
        public ButtonName next()
        { return this.next; }


        /**
         * Returns the name of the enumeration instance (i.e. constant). For {@code ButtonName.FOO} this method would return &quot;FOO&quot;.
         *
         * @return the name of the enumeration instance (i.e. constant)
         */
        public String toString()
        { return this.name; }


        /**
         * Gets the ordinal of the enumeration instance (i.e. constant). This is a convenience method since the {@link #ordinal} property itself is public.
         *
         * @return the ordinal of the enumeration instance (i.e. constant)
         */
        public int getOrdinal()
        { return ordinal; }


        public int getButtonNumber() {return ordinal + 1;}


        public JoystickButton getButton(JoystickDef joystick)
        {
            return joystick.getButton(getButtonNumber());
        }
    }
}
